from bbdata.model import Model
from bbdata.entity import Entity
import json

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine

engine = create_engine('postgresql://bookbrainz:bookbrainz@localhost/bookbrainz')

Session = sessionmaker(bind=engine)
session = Session()

User = Model('../model/User.json')
UserType = Model('../model/UserType.json')
Gender = Model('../model/Gender.json')
Revision = Model('../model/Revision.json')
Model('../model/Disambiguation.json')
Model('../model/Annotation.json')
Model('../model/CreatorType.json')
Model('../model/Alias.json')
Entity('../model/entities/Entity.json')
Creator = Entity('../model/entities/Creator.json')
print Model._known_models
print json.dumps(User.get(4, session, exclude=['password'], populate=['user_type', 'gender']), indent=4)

User.update(4, {"gender": {"id": 1}}, session)
print json.dumps(User.get(4, session, exclude=['password'], populate=['user_type', 'gender']), indent=4)

print json.dumps(User.list(session, exclude=['password'], stub=False), indent=4)


print "entity", json.dumps(Creator.get('15d3b302-6aef-49f5-a184-818ef20ece20', session, populate=['disambiguation', 'gender', 'annotation']), indent=4)
print json.dumps(Creator.list(session, populate=['disambiguation', 'gender', 'annotation'], filters=['begin_date=2014']), indent=4)
