import json
from os.path import basename, splitext

import bbschema
import re
import sqlalchemy.orm.relationships

# TODO: URI attribute type
# TODO: Urlify function

class ParseError(IOError):
    pass

class SchemaFieldError(IOError):
    pass

class UnknownModelError(IOError):
    pass

class ValidationEnum(IOError):
    pass

class FormatError(IOError):
    pass

class NotFoundError(IOError):
    pass

class FilterError(IOError):
    pass

class SortError(IOError):
    pass

class Model(object):
    """ This class provides the ability to access data stored in a BookBrainz
    database. It is initialized with a model definition, describing fields
    and data types, and uses this to access and modify data in the database.
    """

    _known_models = {}

    def __init__(self, definition_filename):
        self._schema_class_name = splitext(basename(definition_filename))[0]
        self._schema_class = getattr(bbschema, self._schema_class_name)

        self._definition = Model._parse_definition(definition_filename)

        self._id_attr = self._definition['_id_attr']
        del self._definition['_id_attr']

        if '_stub_fields' in self._definition:
            self._stub_fields = self._definition['_stub_fields']
            del self._definition['_stub_fields']
        else:
            self._stub_fields = self._definition.keys()

        self._validate_model()
        Model._known_models[self._schema_class_name] = self

    def _get_value(self, schema_object, field_name):
        field_name_parts = field_name.split('.')
        query_object = schema_object
        #print schema_object
        #print field_name_parts
        for part in field_name_parts:
            try:
                query_object = getattr(query_object, part)
            except AttributeError:
                raise SchemaFieldError(
                    'Schema object "{}" has no mapped field "{}"'
                    .format(type(query_object), part)
                )

            if query_object is None:
                return None

        return query_object

    def _set_value(self, schema_object, field_name, field_value):
        field_name_parts = field_name.split('.')
        query_object = schema_object
        schema_class = self._schema_class

        for part in field_name_parts[:-1]:
            try:
                child_object = getattr(query_object, part)
            except AttributeError:
                raise SchemaFieldError(
                    'Schema object "{}" has no mapped field "{}"'
                    .format(type(query_object), part)
                )

            if isinstance(getattr(schema_class, field_name_parts[0]).property,
                   sqlalchemy.orm.relationships.RelationshipProperty):
                schema_class = \
                   getattr(schema_class, field_name_parts[0])\
                   .property.mapper.class_

            if child_object is None:
                # Create a new related object and set child_object
                child_object = schema_class()
                setattr(query_object, part, child_object)

            query_object = child_object

        setattr(query_object, field_name_parts[-1], field_value)

    def _validate_relationship_field_name(self, schema_class, field_name):
        """ This part of field name validation is split out so that it can be
        overloaded in Entity.
        """

        field_name_parts = field_name.split('.')

        if not hasattr(schema_class, field_name_parts[0]):
            raise SchemaFieldError('Class "{}" has no field "{}"'
                                   .format(schema_class, field_name_parts[0]))

        if not isinstance(getattr(schema_class, field_name_parts[0]).property,
                          sqlalchemy.orm.relationships.RelationshipProperty):
            raise SchemaFieldError('Field "{}" is not a relationship property'
                                   .format(schema_class, field_name_parts[0]))

        # Check prescense of remainder of field name parts on related class
        related_class = \
            getattr(schema_class, field_name_parts[0])\
            .property.mapper.class_
        self._validate_field_name(related_class,
                                  '.'.join(field_name_parts[1:]))

    def _validate_field_name(self, schema_class, field_name):
        """ Perform 'static' validation of a schema field name against a schema
        class, following relationships to deal with nested attributes. Can't
        currently check one-to-many or many-to-many relationships.
        """

        field_name_parts = field_name.split('.')
        #print field_name_parts

        if len(field_name_parts) > 1:
            self._validate_relationship_field_name(schema_class, field_name)
        else:
            if not hasattr(schema_class, field_name_parts[0]):
                raise SchemaFieldError('Class "{}" has no field "{}"'
                                       .format(schema_class, field_name_parts[0]))

    def _validate_model(self):
        for field in self._definition:
            field_def = self._definition[field]
            field_name = field_def.get('map', False) or field

            if not field_def.get('many', False):
                self._validate_field_name(self._schema_class, field_name)

            if u'type' not in field_def:
                raise ParseError('"{}" missing type field'.format(field))

            field_type = field_def[u'type']

            if field_type == u'enum':
                # Check that the field has a 'values' attribute
                if u'values' not in field_def:
                    raise ParseError(
                        '{} field is missing values attribute'.format(field)
                    )
            elif field_type == u'object':
                if u'model' not in field_def:
                    raise ParseError(
                        '{} field is missing model attribute'.format(field)
                    )

                if u'_id_attr' not in field_def and not field_def.get(u'many', False):
                    raise ParseError(
                        '{} field is missing id_attr attribute'.format(field)
                    )

    @staticmethod
    def _parse_definition(def_filename):
        try:
            with open(def_filename, 'rb') as def_file:
                base = json.load(def_file)
                if '_inherits' in base:
                    for inherit in base['_inherits']:
                        base.update(Model._known_models[inherit]._definition)
                    del base['_inherits']
                return base
        except IOError:
            return None

    @staticmethod
    def _validate_enum(value, allowed_values):
        # Check that value is an allowed value
        if value is None:
            return None

        if value not in allowed_values:
            raise ValidationEnum(
                '"{}" not in allowed values {}'.format(value, allowed_values)
            )

        return value

    @staticmethod
    def _validate_partial_date(value):
        # Check that value is an allowed value
        if value is None:
            return None

        matches = [
            re.match(r'\d{4}-\d{2}-\d{2}', value),
            re.match(r'\d{4}-\d{2}', value),
            re.match(r'\d{4}', value)
        ]

        if all(m is None for m in matches):
            raise ValidationEnum(
                '"{}" not allowed date'.format(value)
            )

        return value

    def _read_field(self, field, obj, populate=False):
        field_def = self._definition[field]
        field_name = field_def.get('map', False) or field

        field_value = self._get_value(obj, field_name)

        field_type = field_def[u'type']

        if field_type == u'string':
            return unicode(field_value)
        elif field_type == u'number':
            return int(field_value)
        elif field_type == u'enum':
            return Model._validate_enum(field_value, field_def[u'values'])
        elif field_type == u'partial_date':
            if field_value is not None:
                precision = self._get_value(obj, field_name + '_precision')
                field_value = str(field_value)[
                    0:{'YEAR': 4, 'MONTH': 7, 'DAY': 10}.get(precision, 10)
                ]
            return Model._validate_partial_date(field_value)
        elif field_type == u'object':
            if field_value is None:
                return None

            if isinstance(field_value, list):
                if not field_def.get(u'many', False):
                    raise FormatError('List provided for not-many field "{}"'
                                      .format(field_name))

            model_name = field_def[u'model']
            if model_name not in Model._known_models:
                raise UnknownModelError('Model "{}" not found'.format(model_name))
            model = Model._known_models[model_name]

            if populate or (model._id_attr is None):
                if isinstance(field_value, list):
                    return [model.format(v) for v in field_value]
                else:
                    return model.format(field_value)
            else:
                if isinstance(field_value, list):
                    #print model._definition[model._id_attr]

                    return [{'id': getattr(v, model._id_attr)} for v in field_value]
                else:
                    id_attr = field_def[u'_id_attr']
                    id_value = self._get_value(obj, id_attr)

                    return {'id': id_value}
        else:
            return None

    def format(self, obj, exclude=None, populate=None, stub=False):
        if exclude is None:
            exclude = []

        if populate is None:
            populate = []

        return {
            field: self._read_field(field, obj, populate=(field in populate))
            for field in self._definition
            if (field not in exclude and (not stub or field in self._stub_fields))
        }

    def get(self, id, db_session, exclude=None, populate=None):
        """ Get an existing instance of the model. """
        obj = db_session.query(self._schema_class).get(id)

        if obj is None:
            raise NotFoundError('{} with ID {} not found.'
                                .format(self._schema_class, id))

        return self.format(obj, exclude, populate)

    def _filter(self, query, filters):
        if filters is None or not filters:
            return query

        for f in filters:
            # f will be a string containing an expression to filter with
            match = re.match(r'(\w+)([!<=>]+)([\w\.]+)', f)

            if match is None:
                # Raise an exception to say the filter string is badly formed
                raise FilterError('Badly formatted filter expression: "{}"'
                                  .format(f))

            field_def = self._definition.get(match.groups()[0])
            if field_def is None:
                raise FilterError(
                    'Model "{}" has no field "{}"'
                    .format(self._schema_class_name, match.groups()[0])
                )

            field_name = field_def.get('map', False) or match.groups()[0]
            operation = match.groups()[1]
            value = match.groups()[2]

            field_type = field_def['type']

            if field_type == u'string':
                value = unicode(value)
            elif field_type == u'number':
                value = int(value)
            elif field_type == u'enum':
                value = _validate_enum(value, field_def[u'values'])
            elif field_type == u'partial_date':
                value = _validate_partial_date(value)
                # Take partial date and convert to stored date
                value = (value + "-01-01")[:10]
            else:
                raise FormatError("Filtering on non-primitive field types is not supported.")

            schema_attribute = getattr(self._schema_class, field_name)
            if operation == '=':
                return query.filter(schema_attribute == value)
            if operation == '<':
                return query.filter(schema_attribute < value)
            if operation == '>':
                return query.filter(schema_attribute > value)
            if operation == '<=':
                return query.filter(schema_attribute <= value)
            if operation == '>=':
                return query.filter(schema_attribute >= value)
            if operation == '!=':
                return query.filter(schema_attribute != value)

            # Operation not allowed
            raise FilterError('Unrecognised operation: "{}"'
                              .format(operation))

    def _sort(self, query, sort):
        if sort is None:
            return query

        sort_params = sort.split(',')
        if not sort_params[0]:
            raise SortError('Badly formed sort specification: "{}"'
                            .format(sort))

        sort_field = sort_params[0]
        field_def = self._definition.get(sort_field)
        if field_def is None:
            raise SortError(
                'Model "{}" has no field "{}"'
                .format(self._model_name, sort_field)
            )

        if field_def['type'] not in ['string', 'number']:
            raise SortError("Sorting on field types other than string"
                            "and number is not supported.")

        field_name = field_def.get('map', False) or sort_field
        schema_attribute = getattr(self._schema_class, field_name)

        if len(sort_params) == 2:
            if sort_params[1] == 'asc':
                return query.order_by(schema_attribute.asc())
            if sort_params[1] == 'desc':
                return query.order_by(schema_attribute.desc())

            raise SortError('"{}" is not a valid sorting order.'
                            .format(sort_params[1]))
        if len(sort_params) == 1:
            return query.order_by(schema_attribute)

        raise SortError('Too many commas in sorting specification: "{}"'
                        .format(sort))

    def list(self, db_session, offset=None, limit=None, exclude=None, populate=None, filters=None, sort=None, stub=True, query=None):
         """ Get many instances of the model. """
         if query is None:
             query = db_session.query(self._schema_class)

         if offset is not None:
             query = query.offset(offset)

         if limit is not None and limit != -1:
             query = query.limit(limit)

         # Perform sorting of results
         query = self._sort(query, sort)

         # Allow basic filtering (<, >, =, !=, >=, <=)
         query = self._filter(query, filters)

         results = [self.format(obj, exclude, populate, stub) for obj in query.all()]

         return {
             'offset': offset,
             'count': len(results),
             'results': results
         }

    def _write_field(self, field, value, obj):
        field_def = self._definition[field]
        field_name = field_def.get('map', False) or field

        field_type = field_def[u'type']

        if field_type == u'string':
            self._set_value(obj, field_name, unicode(value))
        elif field_type == u'number':
            self._set_value(obj, field_name, int(value))
        elif field_type == u'enum':
            self._set_value(obj, field_name, _validate_enum(value, field_def[u'values']))
        elif field_type == u'object':
            id_attr = field_def[u'_id_attr']
            self._set_value(obj, id_attr, value['id'])

    def new(self, data, db_session, **kwargs):
        """ Create a new instance of the model. """
        obj = self._schema_class()

        for field in data.keys():
            self._write_field(field, data[field], obj)

        db_session.add(obj)
        db_session.commit()

        return (self._read_field(obj, self._id_attr)
                if self._id_attr is None else obj)

    def delete(self, id, db_session):
        """ Delete an existing instance of the model. """
        obj = db_session.query(self._schema_class).get(id)

        if obj is None:
            raise NotFoundError('{} with ID {} not found.'
                                .format(self._model_name, id))

        db_session.delete(obj)
        db_session.commit()

    def update(self, id, data, db_session, **kwargs):
        """ Update an existing instance of the model with new data. """

        obj = db_session.query(self._schema_class).get(id)

        if obj is None:
            raise NotFoundError('{} with ID {} not found.'
                                .format(self._model_name, id))

        data.update(kwargs)

        for field in data.keys():
            self._write_field(field, data[field], obj)

        db_session.commit()
