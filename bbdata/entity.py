#class Entity(object):
#    def __init__(self):
#        """ Initialize"""
#        pass
#
#    def get(bbid):
#        """ Loads an entity by its bbid. """
#        pass
#
#def make_entity_type(name, model):
#    """Create a sub-type of Entity, using some model. """
#

from .model import Model
import bbschema
from os.path import basename, splitext
import json

class Entity(Model):
    def _validate_relationship_field_name(self, schema_class, field_name):
        """ This part of field name validation is split out so that it can be
        overloaded in Entity.
        """

        field_name_parts = field_name.split('.')

        if field_name_parts[0] != 'entity_data':
            super(Entity, self)._validate_relationship_field_name(schema_class,
                                                                 field_name)

    def format(self, obj, exclude=None, populate=None, stub=False, revision=None):
        entity_data = obj.master_revision.entity_data
        obj.entity_data = entity_data

        return super(Entity, self).format(obj, exclude, populate, stub)

    def get(self, id, db_session, exclude=None, populate=None, revision=None):
        """ Get an existing instance of the model. """
        obj = db_session.query(self._schema_class).get(id)

        if obj is None:
            raise NotFoundError('{} with ID {} not found.'
                                .format(self._schema_class, id))

        return self.format(obj, exclude, populate, revision)
