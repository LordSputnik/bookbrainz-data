#!/usr/bin/env python2

from distutils.core import setup

if __name__ == '__main__':
    setup(
        name='bbdata',
        version='1.0',
        description='Classes to facilitate accessing the BookBrainz database',
        author='Ben Ockmore',
        author_email='ben.sput@gmail.com',
        url='https://bitbucket.org/bookbrainz/bookbrainz-data',
        packages=['bbdata'],
        requires=['bbschema'],
        provides=['bbdata'],
    )
